﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="manage_pages.NewPage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" ID="insert_page" 
        ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
    </asp:SqlDataSource>

    <h1>Add New Page</h1>
    <div class="inputrow">
        <asp:Label runat="server">Page Title:</asp:Label>
        <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a page name">
        </asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <asp:Label runat="server">Page Content:</asp:Label>
        <asp:TextBox ID="page_content" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Enter a page content">
        </asp:RequiredFieldValidator>        
    </div>

    <asp:Button Text="Add Page" runat="server" OnClick="AddPage" />
    <!--This Div will show when page is created -->
    <div class="querybox" id="debug" runat="server">
    </div>
    </asp:Content>
