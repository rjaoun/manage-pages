﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace manage_pages
{
    public partial class page_view : System.Web.UI.Page
    {
        private string page_subquery = " SELECT pageid, pagetitle, pagecontent, authorname " +
                                       " FROM Pages left join authors on authors.authorid = pages.authorid ";
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == "" || pageid == null)
            {
                page_name.InnerHtml = "No page is Found!";
                return;
            }
            else
            {
                page_subquery += " WHERE pageid = " + pageid;
                page_select.SelectCommand = page_subquery;
                //page_query.InnerHtml = page_subquery;


                DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

                DataRowView pagerowview = pageview[0];
                string pagename = pageview[0]["pagetitle"].ToString();
                page_name.InnerHtml = pagename;

                string authorname = pageview[0]["authorname"].ToString();
                page_author.InnerHtml = authorname;

                string pagecontent = pageview[0]["pagecontent"].ToString();
                page_content.InnerHtml = pagecontent;

                page_list.DataSource = page_view_Manual_Bind(page_select);
                page_list.DataBind();


            }
        }
        
        protected DataView page_view_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            myview = mytbl.DefaultView;
            return myview;
        }
        protected void DelPage(object sender, EventArgs e)
        {
            string deleteQuery = "DELETE FROM Pages WHERE PAGEID=" + pageid;

            //del_debug.InnerHtml = deleteQuery;
            del_page.DeleteCommand = deleteQuery;
            del_page.Delete();

        }
    }
}