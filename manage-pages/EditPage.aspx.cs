﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace manage_pages
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Edit_Page()
        {

        }
        // We will use PreRender to get the this.pageid in EditPage.aspx
        // This is taken from Christine Example of CRUD.
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            DataRowView pagerow = GetPageInfo(pageid);
            if(pagerow == null)
            {
                page_Name.InnerHtml = "No Page Found!";
                return;
            }
            page_title.Text = pagerow["pagetitle"].ToString();

            page_content.Text = pagerow["pagecontent"].ToString();
        }
        protected void Edit_Page(object sender, EventArgs e)
        {
            string name = page_title.Text;
            string content = page_content.Text;

            string editquery = "Update Pages set pagetitle ='" + name + "'," +
                               "pagecontent='" + content + "'" +
                               "where pageid=" + pageid;
            
            debug.InnerHtml = "The page is edited";

            edit_page.UpdateCommand = editquery;
            edit_page.Update();
        }
        protected DataRowView GetPageInfo(int id)
        {
            string query = "select * from Pages where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if(pageview.ToTable().Rows.Count <1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
    }
}