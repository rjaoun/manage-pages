﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace manage_pages
{
    public partial class pages_menu : System.Web.UI.UserControl
    {
        private string subquery = "SELECT pageid, pagetitle, pagecontent" +
                                  " FROM Pages";
        /*
        private int selected_id;
        public int _selected_id
        {
            get { return selected_id;  }
            set { selected_id = value; }
        }
        */
        protected void Page_Load(object sender, EventArgs e)
        {
            page_list_pick.SelectCommand = subquery;



            string itemList = pages_menu_Manual_Bind(page_list_pick);
            nav_menu.InnerHtml = itemList;

        }
        private string pages_menu_Manual_Bind(SqlDataSource src)
        {
            

            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            string getdata = string.Empty; 

            foreach(DataRowView row in myview)
            {
               /* MenuItem page_item = new MenuItem();
                page_item.Text = row["pageid"].ToString();
                page_item.Text = row["pagetitle"].ToString();
                pagelist.Items.Add(page_item);
                */
                string menulist = row["pagetitle"].ToString();
                string eachlist = "<li><a  href=page_view.aspx?pageid="+ row["pageid"]+ ">" + menulist  +  "</a></li>";
                getdata += eachlist;
            }
            return getdata;
        }

        /*
        protected void Assign_PageID(object sender, EventArgs e)
        {
            _selected_id = int.Parse(page_pick.SelectedValue);
        }*/
    }   
}